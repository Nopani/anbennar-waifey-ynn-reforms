#All scopes are country scope
#All parameters are optional

estate_wolf_command_land_rights = {
	icon = privilege_grant_autonomy
	loyalty = 0.05
	influence = 0.05
	land_share = 5
	max_absolutism = -5
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		governing_capacity = 100
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_wolf_command_logistics_department = {
	icon = privilege_seek_support_of_clergy
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		country_admin_power = 1
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_wolf_command_x = {
	#icon = 
	loyalty = 0.1
	influence = 0.1
	land_share = 0
	max_absolutism = -10
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_wolf_command_officer_operations_training = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		leader_land_manuever = 1
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_wolf_command_recruit_logistician = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	is_valid = {

	}
	
	on_granted = {
		define_advisor = {
			culture = wolf_hobgoblin
			type = quartermaster
			discount = yes
			cost_multiplier  = 0.25
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_wolf_command_management = { # this need more details
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
		if = {
			limit = {
				any_owned_province = {
					has_province_modifier = hob_wolf_war_camp # make this check teh correct province/modifier
				}
			}
			custom_tooltip = "Add a cool modifier in the Wolf War Camp"
		}
		else = {
			custom_tooltip = "Has no effects upon being granted."
		}
	}
	
	on_granted_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_wolf_war_camp } # make this check teh correct province/modifier
			custom_tooltip = estate_wolf_command_management_tt
			hidden_effect = { add_province_triggered_modifier = hob_wolf_management }
		}
	}
	
	on_revoked = {
		custom_tooltip = "Remove the cool modifier in the Wolf War Camp"
	}
	
	on_revoked_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_wolf_management }
			custom_tooltip = revoke_estate_wolf_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_wolf_management }
		}
	}
	
	on_invalid = {
		custom_tooltip = "Remove the cool modifier in the Wolf War Camp"
	}
	
	on_invalid_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_wolf_management }
			custom_tooltip = revoke_estate_wolf_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_wolf_management }
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	modifier_by_land_ownership = {
		development_cost = -0.2
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_wolf_command_ninyu_kikun_jikunin = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	is_valid = {
	}
	
	on_granted = {
		custom_tooltip = estate_wolf_command_ninyu_kikun_jikunin_grant_tt
		capital_scope = {
			add_unit_construction = {
				type = cavalry
				amount = 5
				speed = 1
				cost = 0
			}
		}
	}
	
	penalties = {
	}
	
	benefits = {
		cavalry_shock = 0.1
	}
	
	conditional_modifier = {
		trigger = { faction_in_power = hob_wolf_command }
		modifier = {
			cavalry_cost = -0.2
		}
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}
