
estate_gerunanin_disarmed_populance = {
	#icon = 
	loyalty = 0.05
	influence = -0.10
	land_share = 0
	max_absolutism = 0
	
	can_select = {
		any_owned_province = {
			custom_trigger_tooltip = {
				tooltip = "Culture is Human"
				culture_is_human = yes
			}
		}
	}
	
	is_valid = {
	}
	
	on_granted = {
		custom_tooltip = "All human provinces will stop producing manpower."
		set_country_flag = hob_gerunanin_disarmed_populance_flag	# used to add the modifier when you conquer human provinces
	}
	
	on_granted_province = {
		if = {
			limit = { culture_is_human = yes }
			add_province_modifier = {
				name = hob_gerunanin_disarmed_populance	#-100% local manpower
				duration = -1
			}
		}
	}
	
	on_revoked = {
		clr_country_flag = hob_gerunanin_disarmed_populance_flag
	}
	
	on_revoked_province = {
		if = {
			limit = { has_province_modifier = hob_gerunanin_disarmed_populance }
			remove_province_modifier = hob_gerunanin_disarmed_populance
		}
	}
	
	on_invalid = {
		clr_country_flag = hob_gerunanin_disarmed_populance_flag
	}
	
	on_invalid_province = {
		if = {
			limit = { has_province_modifier = hob_gerunanin_disarmed_populance }
			remove_province_modifier = hob_gerunanin_disarmed_populance
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_gerunanin_wuhyun_rights = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
		if = {
			limit = { has_estate_privilege = estate_gerunanin_disarmed_populance }
			stability = 2
		}
		any_owned_province = {
			culture_group = wuhyun
		}
	}
	
	can_revoke = {
	}
	
	is_valid = {
		any_owned_province = {
			culture_group = wuhyun
		}
	}
	
	on_granted = {
		if = {
			limit = { has_estate_privilege = estate_gerunanin_disarmed_populance }
			add_stability = -2
			hidden_effect = { remove_estate_privilege = estate_gerunanin_disarmed_populance }
			clr_country_flag = hob_gerunanin_disarmed_populance_flag
		}
		custom_tooltip = "All human provinces but Wuhyun ones will stop producing manpower."
		set_country_flag = hob_gerunanin_wuhyun_rights_flag	# used to add the modifier when you conquer human provinces
		hidden_effect = {
			every_owned_province = {
				limit = { has_province_modifier = hob_gerunanin_disarmed_populance }
				remove_province_modifier = hob_gerunanin_disarmed_populance
			}
			every_owned_province = {
				limit = {
					culture_is_human = yes
					NOT = { culture_group = wuhyun }
				}
				add_province_modifier = {
					name = hob_gerunanin_disarmed_populance	#-100% local manpower
					duration = -1
				}
			}
		}
	}
	
	on_revoked = {
		clr_country_flag = hob_gerunanin_wuhyun_rights_flag
		every_owned_province = {
			limit = { has_province_modifier = hob_gerunanin_disarmed_populance }
			remove_province_modifier = hob_gerunanin_disarmed_populance
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_gerunanin_local_traditions = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	can_revoke = {
	}
	
	is_valid = {
	}
	
	on_granted = {
	}
	
	on_revoke = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_gerunanin_directed_trading = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	can_revoke = {
	}
	
	is_valid = {
	}
	
	on_granted = {
	}
	
	on_revoke = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_gerunanin_enlist_naval_experts = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	can_revoke = {
	}
	
	is_valid = {
		any_owned_province = {
			has_port = yes
		}
	}
	
	on_granted = {
	}
	
	on_revoke = {
	}
	
	penalties = {
	}
	
	benefits = {
		navy_tradition_decay = -0.05
		admiral_cost = -0.25
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_gerunanin_autonomous_enclaves = {
	#icon = 
	loyalty = 0.05
	influence = 0.10
	land_share = 0
	max_absolutism = -10
	
	can_select = {
	}
	
	can_revoke = {
	}
	
	is_valid = {
		num_of_subjects = 1
	}
	
	on_granted = {
	}
	
	on_revoke = {
	}
	
	penalties = {
	}
	
	benefits = {
		reduced_liberty_desire = 10
		diplomatic_upkeep = 2
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}