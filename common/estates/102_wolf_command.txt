
estate_wolf_command = {
	icon = 1
	color = { 0 104 210 }
	
	trigger = {
		tag = R62
	}
	
	country_modifier_happy = {
		global_manpower = 10	# +10k flat manpower
		reinforce_speed = 0.20	# +20% reinforce speed
		cavalry_power = 0.15	# +15% cavalry power
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		reinforce_speed = -0.10	# -10% reinforce speed
	}
	
	land_ownership_modifier = {
		wolf_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: �Y10%�!"
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	# Loyalty modifiers
	loyalty_modifier = {
		desc = "Loyalty Modifier: �Y10%�!"
		trigger = {
			#<triggers>
		}
		loyalty = 10.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_wolf_command_land_rights
		estate_wolf_command_logistics_department
		estate_wolf_command_x
		estate_wolf_command_officer_operations_training
		estate_wolf_command_officer_recruit_logistician
		estate_wolf_command_management
		estate_wolf_command_ninyu_kikun_jikunin
	}
	
	agendas = {
		estate_wolf_command_campaign_shamakhad
		estate_wolf_command_hire_advisor
		estate_wolf_command_fire_advisor
		estate_wolf_command_expand_into_x
		estate_wolf_command_complete_conquest_of_x
		estate_wolf_command_build_up_manpower_reserves
	}
	
	influence_from_dev_modifier = 1.0
}