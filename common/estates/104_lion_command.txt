
estate_lion_command = {
	icon = 1
	color = { 66 0 116 }
	
	trigger = {
		tag = R62
	}
	
	country_modifier_happy = {
		global_manpower = 10	# +10k flat manpower
		war_exhaustion = -0.05	# -0.05 monthly war exhaustion
		general_cost = -0.15	# -15% general cost
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		war_exhaustion = 0.05	# +0.05 monthly war exhaustion
	}
	
	land_ownership_modifier = {
		lion_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: "
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	loyalty_modifier = {
		desc = "Loyalty Modifier: "
		trigger = {
			#<triggers>
		}
		loyalty = 10.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_lion_command_land_rights
		estate_lion_command_oratory_schools
		estate_lion_command_x
		estate_lion_command_war_room_funds
		estate_lion_command_exaltations_from_leadership
		estate_lion_command_management
		estate_lion_command_ninyu_kikun_officers
	}
	
	agendas = {
		estate_lion_command_campaign_shamakhad
		estate_lion_command_hire_advisor
		estate_lion_command_fire_advisor
		estate_lion_command_retake_core
		estate_lion_command_recover_abysmal_prestige
		estate_lion_command_improve_prestige
	}
	
	influence_from_dev_modifier = 1.0
}